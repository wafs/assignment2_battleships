# Assignment 2 : Battleships #

This was my second assignment at the University of Sydney for INFO1103. 

I had to write a battleships game and create a basic player AI that could play the game.

* All code in the main file (Battleships.java) was done by me, aside from the initial skeleton code that contained method names. 

* All code in the AI player (wsal5855/plzwork.java) was done by me, aside from the initial skeleton code that contained method names. 

