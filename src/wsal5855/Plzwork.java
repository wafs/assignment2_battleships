package wsal5855;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Deque;
import java.util.HashMap;

import util.Coordinate;
import util.Ship;
import util.ShipPlacement;
import util.ShipPlacement.Direction;
import core.GameConfiguration;
import core.Player;

public class Plzwork implements Player {

	// Variables
	private Coordinate coords;
	private Coordinate lastShot;
	private PlayerState currentState;
	private ShipPlacement shotShip;
	private Coordinate shot;
	private Direction currentDirection;
	
	// Logic 
	enum PlayerState {SCOUTING, HOMING, SEEK_AND_DESTROY, PROCESSING_SHOTQUE};
	boolean[][] hasHit;
	
	// Flags
	private boolean fourCollected;
	private int shotsRemainingAroundTarget;
	private boolean tryOpposite;
	
	// Data Structures
	private Deque<ShipPlacement> coordQueue = new ArrayDeque<ShipPlacement>();
	private ArrayList<ShipPlacement> targets = new ArrayList<ShipPlacement>();
	private ArrayList<Coordinate> coordList;
	private HashMap<Direction, Direction> oppositeDirection  = new HashMap<Direction, Direction>();
	private Coordinate lockedTarget;
	

	public Plzwork() {
		oppositeDirection.put(Direction.NORTH, Direction.SOUTH);
		oppositeDirection.put(Direction.SOUTH, Direction.NORTH);
		oppositeDirection.put(Direction.EAST, Direction.WEST);
		oppositeDirection.put(Direction.WEST, Direction.EAST);
	}

	// get random coordinates so long as the coordinates haven't been used yet
	public Coordinate getRandomCoords() {		

		coords = coordList.remove(coordList.size() - 1);
		hasHit[coords.getX()][coords.getY()] = true;
		
		return coords;
	}
	
	// checks if coordinate is within the grid's size  & a valid location on the grid
	public boolean isValid(Coordinate shot) {
		if (shot.getX() < 0 || shot.getY () < 0 || shot.getX() > (hasHit.length - 1) || shot.getY() > (hasHit.length - 1))
			return false;
		
		return (hasHit[shot.getX()][shot.getY()] == false);
	}
	
	// gets next shot using states to change behaviour
	// SCOUTING, HOMING, PROCESSING_SHOTQUE, SEEK_AND_DESTROY
	@Override
	public Coordinate getNextShot(char[][] myBoard,	char[][] myViewOfOpponentBoard) {
		gotHit = false;
		int lx = lastShot.getX();
		int ly = lastShot.getY();
		
		if(currentState == PlayerState.PROCESSING_SHOTQUE 
		&& myViewOfOpponentBoard[lx][ly] == 'H'){
			currentState = PlayerState.SEEK_AND_DESTROY;
		}


		// SCOUT
		// returns random coordinates.
		// if the player is scouting and the last shot was a hit, it changes to homing mode
		if (currentState == PlayerState.SCOUTING) {
			if (myViewOfOpponentBoard[lx][ly] == 'H') {
				lockedTarget = lastShot;
				currentState = PlayerState.HOMING;
				targets.clear();
			} else {
				shot = getRandomCoords();
				hasHit[lx][ly] = true;
				lastShot = shot;
				return shot;
			}
		}
		
		
		// HOMING				
		// Refine the search area : 
		// collect 4 potential coordinates around the locked target
		// if they're valid, it adds them to queue of shots
		if(currentState == PlayerState.HOMING){
			// COLLECT FOUR
			if(!fourCollected){
				// abuse the use of ShipPlacement to pair up directions and coordinates
				targets.add(new ShipPlacement(new Coordinate(lx, ly + 1), Direction.NORTH));
				targets.add(new ShipPlacement(new Coordinate(lx, ly - 1), Direction.SOUTH));
				targets.add(new ShipPlacement(new Coordinate(lx - 1, ly), Direction.WEST));
				targets.add(new ShipPlacement(new Coordinate(lx + 1, ly), Direction.EAST));
				
				// Validity check
				for (ShipPlacement ship : targets) {
					if (isValid(ship.getBeginning())) {
						coordQueue.add(ship);
						shotsRemainingAroundTarget++;
					}
				}
				
				
				fourCollected = true;
			}
			
			// Start processing shots
			currentState = PlayerState.PROCESSING_SHOTQUE;				
		}
		
		
		// PROCESSING_SHOTQUE
		// Uses shots in the list until a target is hit.
		if(currentState == PlayerState.PROCESSING_SHOTQUE){
			if (shotsRemainingAroundTarget > 0){
				shotsRemainingAroundTarget--;
				shotShip = coordQueue.getFirst();
				coordQueue.removeFirst();

				hasHit[shotShip.getBeginning().getX()][shotShip.getBeginning().getY()] = true;
				lastShot = shotShip.getBeginning();
				return shotShip.getBeginning();
			} else {
				fourCollected = false;
				currentState = PlayerState.SCOUTING;
			}
		}
		
		// SEEK_AND_DESTROY
		// Attempts to destroy the rest of the ship after it's 
		// been located.
		if (currentState == PlayerState.SEEK_AND_DESTROY){
			// get direction
			currentDirection = shotShip.getDirection();
			
			if(myViewOfOpponentBoard[lx][ly] != 'H'){
				// when an empty tile is hit, go back and try the opposite direction
				// when an empty tile is hit again, go back to scouting
				if(!tryOpposite){
					lastShot = lockedTarget;
					lx = lastShot.getX();
					ly = lastShot.getY();
					currentDirection = oppositeDirection.get(shotShip.getDirection());
					tryOpposite = true;
				} else {
					currentState = PlayerState.PROCESSING_SHOTQUE;
				}
			}
			
			switch (currentDirection) {
			case NORTH:
				// return new shot
				shot = new Coordinate(lx, ly + 1);
				break;
			case SOUTH:
				// return new shot
				shot = new Coordinate(lx, ly - 1);
				break;
			case EAST:
				// return new shot
				shot = new Coordinate(lx + 1, ly);
				break;
			case WEST:
				// return new shot
				shot = new Coordinate(lx - 1 , ly);
				break;
			}
				
		}
		if (!isValid(shot)){
			shot = getRandomCoords();
		}
	
		hasHit[shot.getX()][shot.getY()] = true;
		lastShot = shot;
		return shot;
	}

	boolean ThreeDone = false;
	private boolean gotHit;

	@Override
	// Hard coded ship placement
	public ShipPlacement getShipPlacement(Ship ship, char[][] myBoard) {
		int shipLength = ship.getShipLength();
		Coordinate beginning = null;
		Direction direction = null;

		if (shipLength == 5) {
			beginning = new Coordinate(3, 2);
			direction = Direction.EAST;
		}

		if (shipLength == 4) {
			beginning = new Coordinate(5, 6);
			direction = Direction.NORTH;
		}

		if (shipLength == 3 && ThreeDone == true) {
			beginning = new Coordinate(1, 9);
			direction = Direction.EAST;
		}

		if (shipLength == 3 && ThreeDone == false) {
			beginning = new Coordinate(5, 0);
			direction = Direction.EAST;
			ThreeDone = true;
		}

		if (shipLength == 2) {
			beginning = new Coordinate(8, 9);
			direction = Direction.SOUTH;
		}

		ShipPlacement placement = new ShipPlacement(beginning, direction);
		return placement;
	}

	@Override
	public void notify(String message) {
		System.out.println(message);
		if(message.contains("HIT")){
			gotHit = true;			
		}
		
		if(message.contains("SUNK") && gotHit){
			currentState = PlayerState.SCOUTING;
		}
	}


	@Override
	public void newGame(String opponent, GameConfiguration config) {
		// Will this finally work?!
		gotHit = false;
		hasHit = new boolean[10][10];
		shotsRemainingAroundTarget = 0;
		lastShot = new Coordinate(0, 0); 
		currentState = PlayerState.SCOUTING;

		// Generate all possible coordinates and throw them in list
		// shuffle list and now we have "random" coordinates.
		coordList = new ArrayList<Coordinate>();
		for (int i = 0; i < hasHit.length; i++) {
			for (int j = 0; j < hasHit[0].length; j++) {
				coordList.add(new Coordinate(i, j));
			}
		}
		
		Collections.shuffle(coordList);

		coords = coordList.get(coordList.size()-1);
		coordList.remove(coordList.size()-1);

		// 
		System.out.println("You are versing : " + opponent);
		System.out.println("## Game Configuration ##");
		System.out.printf("Grid Size : %d x %d\n", config.getGridWidth(),
				config.getGridHeight());
		for (Ship ship : config.getShips()) {
			System.out.println("Ship Name : " + ship.getShipHandle()
					+ " , Length : " + ship.getShipLength());
		}
	}
}