package core;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.TreeSet;

import util.Coordinate;
import util.Ship;
import util.ShipPlacement;
import util.ShipPlacement.Direction;
import util.ShipPlacementException;
import wsal5855.Plzwork;

/**
 * Battleships class.
 * 
 * Fill in the methods. They are ordered in the order you should code them in.
 * 
 * NOTES: * Board states: * empty spot is denoted by a 0 ( e.g. char a = 0; ).
 * This is the default value for a char. * ship segments that are not hit are
 * denoted by a single lower case character that equals their ship handle. *
 * ship segments that are hit are denoted by a single UPPER case character that
 * equals their ship handle. * misses are denoted by a '.'
 * 
 * @author ________
 * 
 */
public class BattleShips {

	private static int turns;
	private GameConfiguration config;
	private Player player1;
	private Player player2;
	private ArrayList<Player> playerArray;
	private char[][] board1;
	private char[][] board2;
	char[][][] boardArray;
	private Collection<Ship> p1Ships;
	private Collection<Ship> p2Ships;
	int maxTurns;

	/**
	 * #0 We advise you to implement this first
	 * 
	 * Constructor
	 * 
	 * Initialize all instance variables here.
	 * 
	 * @param config
	 *            - the configuration for this player
	 * @param player1
	 *            - one player
	 * @param player2
	 *            - other player
	 */

	public BattleShips(GameConfiguration config, Player player1, Player player2) {
		this.config = config;

		// players //
		this.player1 = player1;
		this.player2 = player2;
		playerArray = new ArrayList<Player>();
		playerArray.add(player1);
		playerArray.add(player2);

		// boards //
		// Use [height X width] to conform to cartesian coordinates
		board1 = new char[this.config.getGridHeight()][this.config.getGridWidth()];
		board2 = new char[this.config.getGridHeight()][this.config.getGridWidth()];
		boardArray = new char[][][] { board1, board2 };

		// ships //
		p1Ships = this.config.getShips();
		p2Ships = this.config.getShips();
		maxTurns = board1.length * board1[0].length;

	}

	/**
	 * 
	 * Checks if Coordinate is Valid by making sure X & Y positions lie within the grid's coordinates. 
	 * 
	 *  
	 * @param x
	 *            - the x coordinate
	 * @param y
	 *            - the y coordinate
	 * @return true - coordinate is valid
	 * @return false - coordinate is invalid
	 */

	protected boolean isValidCoordinate(int x, int y) {
		if ((x >= 0 && x <= this.config.getGridHeight() - 1)
				&& (y >= 0 && y <= this.config.getGridWidth() - 1)) {
			return true;
		}
		return false;
	}

	/**
	 * #2
	 * 
	 * Checks if shot is valid by testing to see if it's the board's X & Y valid positions. 
	 *  - Is this useful? The method isValidCoordinate does exactly the same thing. 
	 * @param shot
	 *            - the shot being taken
	 * @return true - shot is valid
	 * @return false - the shot is invalid
	 */
	protected boolean isValidShot(Coordinate shot) {
		if (shot == null) {
			return false;
		}
		return isValidCoordinate(shot.getX(), shot.getY());
	}
 
	/**
	 * Gets the char at the selected player's X & Y position. 
	 * 
	 * @param player
	 *            - the player whose board you are referring to (0 for player 1,
	 *            1 for player 2)
	 * 
	 * @param x
	 *            - the x coordinate
	 * @param y
	 *            - the y coordinate
	 * 
	 * @return the character in the cell (-1 if the coordinates are invalid)
	 */
	protected char getCellState(int player, int x, int y) {

		// need to return -1 if invalid coordinates
		if (isValidCoordinate(x, y)) {
			return boardArray[player][x][y];
		} else {
			return (char) -1;
		}
	}

	/**
	 * #4
	 * 
	 * Function to resolve a shot. 
	 * 
	 * Selects enemy player , and current player's shot coordinates.
	 * If the shot is in a valid location, it then does further tests to 
	 * see if the char in that cell is :<br/>
	 * <ul>
	 * 	<li>Lowercase : changes to Uppercase, notify of a hit
	 * 	<li>0 : changes to a '.', notify a miss
	 * 	<li> Uppercase or a '.' : Do nothing, as they're hitting something they've already hit.
	 * </ul>
	 * @param shot
	 *            - the current shot
	 * @param player
	 *            - the player SHOOTING
	 */
	protected void resolveShot(Coordinate shot, int player) {
		// using the caret operator for XOR, so I can simply get the opposite player
		Player currentPlayer = playerArray.get(player);
		Player enemyPlayer = playerArray.get(player ^ 1);
		char enemyCellState = getCellState(player ^ 1, shot.getX(), shot.getY());
		char[][] enemyBoard = boardArray[player ^ 1];

		int getX = shot.getX();
		int getY = shot.getY();
		boolean validShot = isValidShot(shot);

		// process shot validity
		if (validShot) {

			// if an undamaged ship segment is hit
			if (Character.isLowerCase(enemyCellState)) {

				// make uppercase
				enemyBoard[shot.getX()][shot.getY()] = Character.toUpperCase(enemyBoard[shot.getX()][shot.getY()]);
				currentPlayer.notify("HIT (" + getX + "," + getY + ")");

				
				// check if the ship that was just hit still exists,
				// if not, notify players that the ship has sunk
				int counter = 0;
				for (int i = 0; i < enemyBoard.length; i++) {
					for (int j = 0; j < enemyBoard[0].length; j++) {
						if (enemyBoard[i][j] == enemyCellState) {
							counter++;
						}
					}
				}
				if (counter == 0) {
					player1.notify("SUNK " + enemyCellState);
					player2.notify("SUNK " + enemyCellState);
				}
			}

			// if the shot whiffs
			else if (enemyCellState == 0) {
				// make it '.'
				boardArray[player ^ 1][shot.getX()][shot.getY()] = 46;
				enemyPlayer.notify("MISS (" + getX + "," + getY + ")");

			}

			// if it hits a damaged segment or a known empty tile
			else if (Character.isUpperCase(enemyCellState)
					|| enemyCellState == '.') {
				// do nothing (miss)
			}
		}
	}

	/**
	 * #5
	 * 
	 * Function to resolve a ship placement.
	 * 
	 * Checks validity, and if valid it will place a ship if it is not null or if 
	 * the square isn't already occupied, or partially over the edge.
	 * 
	 * If the ship area is already occupied, every ship underneath the ship will be sunk 
	 * and the ship won't be placed. 
	 * 
	 * If the ship is partially over the edge, the ship is not placed either. 
	 * 
	 * 
	 * @param shipPlacement
	 *            - the ship placement (starting coordinate and direction)
	 * @param ship
	 *            - the ship being placed
	 * @param player
	 *            - the player placing the ship
	 * @throws ShipPlacementException
	 *             - thrown when there is a placement problem
	 * @see ShipPlacementException
	 */
	protected void placeShip(Ship ship, ShipPlacement shipPlacement, int player)
			throws ShipPlacementException {
		

		if (shipPlacement == null || ship == null) {
			throw new ShipPlacementException("shipPlacement is null!"); 
		}

		
		char handle = ship.getShipHandle();
		int length = ship.getShipLength();

		Coordinate beginning = shipPlacement.getBeginning();
		int getX = beginning.getX();
		int getY = beginning.getY();
		Direction direction = shipPlacement.getDirection();

		char[][] playerBoard = boardArray[player];

		// creates an array of potential tiles, then checks against these tiles to 
		// see what's inside them to handle already placed ships / invalid tiles.
		char[] proposedTiles = new char[length];
		int[] xPos = new int[length];
		int[] yPos = new int[length];

		// fills proposedTiles with chars of the potential X and Y 
		switch (direction) {
		case NORTH:
			for (int i = 0; i < length; i++) {
				proposedTiles[i] = getCellState(player, getX, getY + i);
				xPos[i] = getX;
				yPos[i] = getY + i;
			}
			break;
		case SOUTH:
			for (int i = 0; i < length; i++) {
				proposedTiles[i] = getCellState(player, getX, getY - i);
				xPos[i] = getX;
				yPos[i] = getY - i;
			}
			break;
		case EAST:
			for (int i = 0; i < length; i++) {
				proposedTiles[i] = getCellState(player, getX + i, getY);
				xPos[i] = getX + i;
				yPos[i] = getY;
			}
			break;
		case WEST:
			for (int i = 0; i < length; i++) {
				proposedTiles[i] = getCellState(player, getX - i, getY);
				xPos[i] = getX - i;
				yPos[i] = getY;
			}
			break;
		}


		// check if a tile is invalid, if it is then it has run aground
		char fail_char = (char) -1;
		for (char item : proposedTiles) {
			if (item == fail_char) {
				playerArray.get(player).notify("SHIP " + handle
						+ " RAN AGROUND");
				throw new ShipPlacementException("SHIP " + handle
						+ " RAN AGROUND");
			}
		}

		// check if ships are in the proposedTiles, if there are then it
		// sinks them all and notifies the player.
		boolean sank = false;
		
		TreeSet<Character> charTree = new TreeSet<Character>();
		for (char item : proposedTiles) {
			if (Character.isLetter(item)) {
				sank = true;
				char placedShip = item;
				// add ship handles to a TreeSet (to avoid duplicates) to build the 
				// notification string.
				charTree.add(item);
				
				// replace handle with 0 for the ships already placed in proposed area.
				for (int i = 0; i < playerBoard.length; i++) {
					for (int j = 0; j < playerBoard[0].length; j++) {
						if (playerBoard[i][j] == placedShip) {
							playerBoard[i][j] = 0;
						}
					}
				}

			}

		}

		if (sank) {
			// create the string of sank ships from TreeSet
			// regex to sanitise the string of [ ] and space
			// notify player and throw ShipPlacementException
			String charString = charTree.toString().replaceAll("[\\[\\] ]", "");
			playerArray.get(player).notify("SHIP " + handle
					+ " RAN OVER SHIP " + charString);
			throw new ShipPlacementException("SHIP " + handle
					+ " RAN OVER SHIP " + charString);
		}

		// place ship
		for (int i = 0; i < xPos.length; i++) {
			playerBoard[xPos[i]][yPos[i]] = handle;
		}
	}

	/**
	 * #6
	 * 
	 * Function to get the enemy view of a player's board

	 * Generates the enemy board. 
	 * If uppercase : Shows 'H'
	 * If lower case, show 0
	 * Otherwise show whatever is there. 
	 *
	 * @param player
	 *            - the player whose board you are going to view
	 * @return the view (contains only 'H', '.' and 0). Char Array.
	 */
	protected char[][] getEnemyView(int player) {
		// creates and fills a new 2d Char Array, and returns it to the player with correct values.
		char[][] enemyView = new char[boardArray[player].length][boardArray[player][0].length];

		for (int i = 0; i < enemyView.length; i++) {
			for (int j = 0; j < enemyView[0].length; j++) {
				if (Character.isUpperCase(boardArray[player][i][j])) {
					enemyView[j][i] = 'H';
				}

				else if (Character.isLowerCase(boardArray[player][i][j])) {
					enemyView[j][i] = 0;

				} else if ((boardArray[player][i][j]) == '.') {
					enemyView[j][i] = '.';
				} else {
					enemyView[j][i] = boardArray[player][i][j];
				}
			}
		}
		return enemyView;
	}

	/**
	 * #7
	 * 
	 * Function to start and run the game.
	 * 
	 * @return the player that has won (null if it was a draw)
	 * 
	 */
	int totalShipLength = 0;

	public Player run() {
		boolean gameOver = false;

		// Introduction //
		// Players are notified of each other, and their current ship configuration. 
		player1.newGame(player2.getClass().getCanonicalName(), config);
		player2.newGame(player1.getClass().getCanonicalName(), config);

		// Placement Loop //

		// Keep placing ships until we've run out of ships. 
		Iterator<Ship> p1Itr = p1Ships.iterator();
		Iterator<Ship> p2Itr = p2Ships.iterator();
		while (p1Itr.hasNext()) {
			// get next ship
			Ship ship1 = p1Itr.next();
			Ship ship2 = p2Itr.next();

			totalShipLength += ship1.getShipLength();

			// get proposed placements
			ShipPlacement p1Placement = player1.getShipPlacement(ship1, board1);
			ShipPlacement p2Placement = player2.getShipPlacement(ship2, board2);

			// check validity of placement
			try {
				placeShip(ship1, p1Placement, 0);
			} catch (ShipPlacementException e) {
				player1.notify(e.getMessage());
			}

			try {
				placeShip(ship2, p2Placement, 1);
			} catch (ShipPlacementException e) {
				player2.notify(e.getMessage());
			}
		}

		// Gameplay loop //
		turns = 0;
		while (!gameOver && turns < maxTurns) {
			turns++;
			// Get Proposed Shots
			Coordinate p1Shot = player1.getNextShot(board1, getEnemyView(1));
			Coordinate p2Shot = player2.getNextShot(board2, getEnemyView(0));

			// Process Shots & Their Validity
			resolveShot(p1Shot, 0);
			resolveShot(p2Shot, 1);

			// Check for game win by looking for all lowercase chars
			// If someone has no more lowercase chars on their board
			// the opponent wins as all the ships are sunk.
			int p1Ships = 0;
			int p2Ships = 0;
			for (int i = 0; i < this.config.getGridHeight(); i++) {
				for (int j = 0; j < this.config.getGridWidth(); j++) {
					if (Character.isLowerCase(board1[i][j])) {
						p1Ships++;
					}

					if (Character.isLowerCase(board2[i][j])) {
						p2Ships++;
					}
				}
			}

			System.out.println(turns);
			if (p1Ships == 0 || p2Ships == 0){
				System.out.println(turns);
			}

			// If won/draw, notify players
			// If draw
			if (p1Ships + p2Ships == 0) {
				player1.notify("DRAW");
				player2.notify("DRAW");
				gameOver = true;
				return null;
			// If player 2 wins
			} else if (p1Ships == 0) {
				player1.notify("LOSE");
				player2.notify("WIN");
				gameOver = true;
				return player2;
			// If player 1 wins
			} else if (p2Ships == 0) {
				player1.notify("WIN");
				player2.notify("LOSE");
				gameOver = true;
				return player1;
			} 
			
			// otherwise repeat turn loop

		}
		return null;
	}
	
	public static void main(String[] args) throws ShipPlacementException {
		Collection<Ship> shipArray = new ArrayList<Ship>();
		shipArray.add(new Ship('a', 5));
		shipArray.add(new Ship('b', 4));
		shipArray.add(new Ship('c', 3));
		shipArray.add(new Ship('d', 3));
		shipArray.add(new Ship('e', 2));

		Player plyr1 = new Plzwork();
		Player plyr2 = new Plzwork();

		GameConfiguration cfg = new GameConfiguration(10, 10, shipArray);
		BattleShips game = new BattleShips(cfg, plyr1, plyr2);
		
		plyr1.newGame("player2", cfg);
		plyr2.newGame("plyr1", cfg);

		game.run();
}
}