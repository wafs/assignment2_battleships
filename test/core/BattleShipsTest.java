package core;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Collection;

import org.junit.Before;
import org.junit.Test;

import core.BattleShips;
import core.GameConfiguration;
import core.Player;
import util.Coordinate;
import util.Ship;
import util.ShipPlacement;
import util.ShipPlacement.Direction;
import util.ShipPlacementException;
import wsal5855.PrincessKenny;

public class BattleShipsTest {
	
	Collection<Ship> ships;
	Ship s1,s2,s3,s4,s5;
	Player player1,player2;
	GameConfiguration config;
	BattleShips b;
	Coordinate shot;
	
	@Before	
	public void setUp(){
		ships = new ArrayList<Ship>();
		s1 = new Ship('a', 5);
		s2 = new Ship('b', 4);
		s3 = new Ship('c', 3);
		s4 = new Ship('d', 3);
		s5 = new Ship('e', 2);
		
		ships.add(s1);
		ships.add(s2);
		ships.add(s3);
		ships.add(s4);
		ships.add(s5);
		
		
		
		player1 = new PrincessKenny();
		player2 = new PrincessKenny();
		
		
		config = new GameConfiguration(10,10,ships);
		b = new BattleShips(config, player1, player2);
	}

	@Test
	public void testIsValidCoordinate() {
		assertTrue("Valid Coordinate [1,1] ", b.isValidCoordinate(1,1));
		assertTrue("Valid Coordinate [0,0]" , b.isValidCoordinate(0,0));
		assertTrue("Valid Coordinate [9,9]" , b.isValidCoordinate(9,9));
	}
	
	@Test
	public void testIsInValidCoordinateNegative() {
		assertFalse("Negative Invalid Coordinate [-3,-51] ", b.isValidCoordinate(-3, 1-51));
	}
	
	@Test
	public void testIsInValidCoordinatePositive() {
		assertFalse("Positive Invalid Coordinate [11,11]", b.isValidCoordinate(11,11));
		assertFalse("Positive Invalid Coordinate [63454,233]", b.isValidCoordinate(63454,233));
	}
	
	@Test
	public void testIsInValidAndValidCoordinate() {
		assertFalse("Invalid & Valid [-20,5]", b.isValidCoordinate(-20,5));
		assertFalse("Invalid & Valid [7,-12]", b.isValidCoordinate(7,-12));
		assertFalse("Invalid & Valid [500,5]", b.isValidCoordinate(500,5));
		assertFalse("Invalid & Valid [5,123]", b.isValidCoordinate(5,123));

	}
	
	@Test
	public void testIsValidShot() {
		shot = new Coordinate(2, 7);
		assertTrue(b.isValidShot(shot));
		shot = new Coordinate(-5,-1);
		assertFalse(b.isValidShot(shot));		
	}

	@Test
	public void testGetCellState() {
		assertTrue("Expecting 0", b.getCellState(0, 1, 1) == 0);
		assertTrue("Expecting (char)-1", b.getCellState(0, -5, -5) == (char)-1);
		assertTrue("Expecting (char)-1", b.getCellState(1, 50, 40) == (char)-1);
		
	}

	@Test
	public void testResolveShot() {
		shot = new Coordinate(5, 5);
		b.resolveShot(shot, 1);
		assertTrue(b.boardArray[0][5][5] == '.');
	}

	@Test
	public void testPlaceShip() {
		Coordinate beginPos = new Coordinate(0,0);
		ShipPlacement placement = new ShipPlacement(beginPos, Direction.NORTH);
		try {
			b.placeShip(s1, placement, 0);
		} catch (ShipPlacementException e) {
			e.printStackTrace();
		}
		assertTrue(b.boardArray[0][0][0] == 'a');
		assertTrue(b.boardArray[0][0][1] == 'a');
		assertTrue(b.boardArray[0][0][2] == 'a');
		assertTrue(b.boardArray[0][0][3] == 'a');
		assertTrue(b.boardArray[0][0][4] == 'a');
		assertTrue(b.boardArray[0][0][5] == 0);
		
		// Placing a boat on top of the first
		beginPos = new Coordinate(0,0);
		placement = new ShipPlacement(beginPos, Direction.EAST);
		
		try {
			b.placeShip(s2, placement, 0);
		} catch (ShipPlacementException e) {
			e.printStackTrace();
		}
		
		assertTrue(b.boardArray[0][0][0] == 0);
		assertTrue(b.boardArray[0][0][1] == 0);
		assertTrue(b.boardArray[0][0][2] == 0);
		assertTrue(b.boardArray[0][0][3] == 0);
		assertTrue(b.boardArray[0][0][4] == 0);
		
		// Placing it in another area
		beginPos = new Coordinate(1,0);
		placement = new ShipPlacement(beginPos, Direction.EAST);
		
		try {
			b.placeShip(s2, placement, 0);
		} catch (ShipPlacementException e) {
			e.printStackTrace();
		}
		
		assertTrue(b.boardArray[0][1][0] == 'b');
		assertTrue(b.boardArray[0][2][0] == 'b');
		assertTrue(b.boardArray[0][3][0] == 'b');
		assertTrue(b.boardArray[0][4][0] == 'b');
		assertTrue(b.boardArray[0][5][0] == 0);
	}
	
	

	@Test
	public void testGetEnemyView() {
		// SCENARIO //
		// Player1 places a 5 length ship named 'a'
		// in the bottom left corner @ 0,0
		// player2 now wants to view the ship
		//
		Coordinate beginPos = new Coordinate(0, 0);
		ShipPlacement placement = new ShipPlacement(beginPos, Direction.NORTH);
		try {
			b.placeShip(s1, placement, 0);
		} catch (ShipPlacementException e) {
			e.printStackTrace();
		}
		
		char[][] enemyView = b.getEnemyView(0);
		
		assertTrue(enemyView[0][0] == 0);
		assertTrue(enemyView[0][1] == 0);
		assertTrue(enemyView[0][2] == 0);
		assertTrue(enemyView[0][3] == 0);
		assertTrue(enemyView[0][4] == 0);
		
		// Player2 has now shot player1's ship
		shot = new Coordinate(0,2);
		b.resolveShot(shot, 1);
		
		enemyView = b.getEnemyView(0);
		assertTrue(enemyView[0][0] == 0);
		assertTrue(enemyView[0][1] == 0);
		assertTrue(enemyView[0][2] == 'H');
		assertTrue(enemyView[0][3] == 0);
		assertTrue(enemyView[0][4] == 0);
		
		for (int j = enemyView.length - 1; j >= 0 ; j--) {
			for (int i = 0; i < enemyView[0].length; i++) {
				if(Character.isAlphabetic(enemyView[i][j]) || enemyView[i][j] == '.'){
					System.out.printf("( %c ) ", enemyView[i][j]);
				} else {
					System.out.printf("(%d,%d) ", i,j);
			}}
			System.out.println();

		}

		// Player2 now misses his shot, and the view updates
		shot = new Coordinate(1,0);
		b.resolveShot(shot,1);
		
		enemyView = b.getEnemyView(0);
		assertTrue(enemyView[1][0] == '.');
		
		//Player2 now shoots Player1's ship down entirely
		shot = new Coordinate(0,0);
		b.resolveShot(shot,1);
		shot = new Coordinate(0,1);
		b.resolveShot(shot,1);
		shot = new Coordinate(0,2);
		b.resolveShot(shot,1);
		shot = new Coordinate(0,3);
		b.resolveShot(shot,1);
		shot = new Coordinate(0,4);
		b.resolveShot(shot,1);
		
		enemyView = b.getEnemyView(0);
		
		assertTrue(enemyView[0][3] == 'H');
		
		for (int j = enemyView.length - 1; j >= 0 ; j--) {
			for (int i = 0; i < enemyView[0].length; i++) {
				if(Character.isAlphabetic(enemyView[i][j]) || enemyView[i][j] == '.'){
					System.out.printf("( %c ) ", enemyView[i][j]);
				} else {
					System.out.printf("(%d,%d) ", i,j);
			}}
			System.out.println();

		}

	}

	@Test
	public void testRun(){
		//System.out.println(b.run());

		// p2 win case
		
		// neither win
	}
	
	@Test
	public void testPlaceShipOnEdge() {
		Coordinate beginPos = new Coordinate(0,0);
		ShipPlacement placement = new ShipPlacement(beginPos, Direction.SOUTH);
		try {
			b.placeShip(s1, placement, 0);
		} catch (ShipPlacementException e) {
			e.printStackTrace();
		}
		
		assertTrue(b.boardArray[0][0][0] == 0);
	}
	
	
}
